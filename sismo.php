<?php
    # Agregar los datos necesarios a las siguientes variables
    $token = "TOKEN";
    $channel = "CHANNEL";
    $here_id = "HERE_ID";
    $here_code = "HERE_CODE";

    # Obtener el código HTML de la página principal de Funvisis
    $ch = curl_init("http://www.funvisis.gob.ve/index.php");
    curl_setopt($ch,CURLOPT_REFERER,'http://www.cantv.com.ve');
    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux i686; rv:32.0) Gecko/20100101 Firefox/40.0');
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
    curl_setopt($ch,CURLOPT_FRESH_CONNECT,TRUE);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,50);
    curl_setopt($ch,CURLOPT_TIMEaOUT,300);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    $data = curl_exec ($ch);
    curl_close ($ch);

    # Extraer la data necesaria del código obtenido
	$re = '/Fecha:&nbsp;<\/b>([^\n]*)\n[^\n]*\n[\t ]*([^ ]*)[^\n]*\n[^\n]*\n[\t ]*([^\n]*)\n[^\n]*\n[\t ]*([^\n]*)\n[^\n]*\n[\t ]*([^ ]*)[^\n]*\n[^\n]*\n[\t ]*([^ ]*)[^\n]*\n[^;]*;([^<]*)/';
	preg_match_all($re, $data, $matches);
	$fecha = $matches[1][0];
	$hora = $matches[2][0];
	$magnitud = $matches[3][0];
	$profundidad = str_replace("&nbsp;", " ", $matches[4][0]);
	$latitud = $matches[5][0];
	$longitud = $matches[6][0];
	$epicentro = $matches[7][0];

    # Crear el texto que se va a enviar
    $text = "$fecha $hora\n";
    $text .= "Magnitud: $magnitud\n";
    $text .= "Profundidad: $profundidad\n";
    $text .= "Ubicación: $latitud, $longitud\n";
    $text .= "Epicentro: $epicentro";

    # Obtener el contenido del archivo de texto
    $uri = "sismo.txt";
    $file = fopen($uri, 'r');
    $output = fread($file,filesize($uri) + 1);
    $key = "$fecha$hora";
    fclose($file);
    
    # Si el contenido es diferente es que el evento es nuevo
    if ($output != $key) {
        $file = fopen($uri, 'w');
        fwrite($file, $key);
        fclose($file);
        $params = array(
            "chat_id" => $channel,
            "parse_mode" => "Markdown",
            "photo" => "https://image.maps.cit.api.here.com/mia/1.6/mapview?app_id=$here_id&app_code=$here_code&c=$latitud,$longitud&z=8&u=6m&t=2&w=400&h=600&pip",
            "caption" => $text
        );
        echo sendMethod($token, "sendPhoto", $params);
    }

    function sendMethod($token, $method, $params = array()){
        $ch = curl_init("https://api.telegram.org/bot$token/$method");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux i686; rv:32.0) Gecko/20100101 Firefox/40.0');
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($ch,CURLOPT_FRESH_CONNECT,TRUE);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,50);
        curl_setopt($ch,CURLOPT_TIMEOUT,300);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        return curl_exec($ch);
    }
